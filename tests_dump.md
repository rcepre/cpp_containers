# TESTS RESUME 
###`ft::vector<>` TestCase coplien functions
**PASSED** -   0.109ms:  
- [x] copy constructor act like std::vector
- [x] ft and stl vectors have same contents, size and capacity.
- [x] assignement operator act like std::vector (_reversed, it's OK_)
- [x] ft and stl vectors have same contents, size and capacity.
- [x] move assignement operator destroy itself then just steal the memory.
- [x] the other object has been ghosted
- [x] the other object data is NULL
- [x] move constructor just steal the memory, no calls
- [x] the other object has been ghosted
- [x] the other object data is NULL

🚀 **10/10 tests passed in 0.109ms**