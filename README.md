# cpp_containers

[in progress]

Implemention of some containers of the C++ STL.

The aim of this project is to understand in depth how STL containers work, but also to gain skills on the use of templates, memory allocation (placement new ... ), traits and other interesting concepts .

## Containers

- vector
- array
- deque [todo]
- list [in progress]
- map [todo]


# Tests

Unit tests are written with the help of an header based test utility (Tester.hpp) written for the project, not very elegant, but it works ... . 
I try to test as many features as possible, while staying consistent with the initial behavior of the STL.

#### run tests

```
# Build:
$ cmake .
$ make
# Run:
$ ./cpp_containers_tests


